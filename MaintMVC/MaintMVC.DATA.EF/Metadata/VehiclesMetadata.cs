﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintMVC.DATA.EF.Metadata
{
    public class VehiclesMetadata
    {
        //public int VehicleID { get; set; }


        public string VehicleYear { get; set; }


        public string VehicleMake { get; set; }


        public string VehicleModel { get; set; }


        public string VehicleColor { get; set; }


        public int VehicleMileage { get; set; }


        public bool is4wd { get; set; }


        public string VehiclePhoto { get; set; }


        public string SpecialNotes { get; set; }


        public System.DateTime DateAdded { get; set; }

    }
}
