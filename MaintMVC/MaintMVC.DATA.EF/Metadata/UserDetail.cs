//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MaintMVC.DATA.EF.Metadata
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserDetail
    {
        public string UserID { get; set; }
        public string OwnerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int VehicleID { get; set; }
        public string VehicleYear { get; set; }
        public string VehicleMake { get; set; }
        public string VehicleModel { get; set; }
    }
}
